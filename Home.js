function info(props){ //props have to be a Student.
    return(<table>
                <tr>
                    <td> نام: </td>
                    <td> {props.getName()} </td>
                </tr>
                <tr>
                    <td> نام خانوادگی: </td>
                    <td> {props.getSecondName()} </td>
                </tr>
                <tr>
                    <td> شماره دانشجویی: </td>
                    <td> {props.getStudentId()} </td>
                </tr>
                <tr>
                    <td> تاریخ تولد: </td>
                    <td> {props.getBirthDate()} </td>
                </tr>
                <tr>
                    <td> معدل کل: </td>
                    <td> {props.getGPA()} </td>
                </tr>
                <tr>
                    <td> واحد گذرانده: </td>
                    <td> {props.getNumOfPassedUnits()} </td>
                </tr>

                <tr>
                    <td> دانکشده: </td>
                    <td> {props.getFaculty()} </td>
                </tr>
                <tr>
                    <td> رشته: </td>
                    <td> {props.getField()} </td>
                </tr>
                <tr>
                    <td> مقطع: </td>
                    <td> {props.getLevel()} </td>
                </tr>
            </table>
            <p id="status">{props.getStatus()}</p>);
}

function courseInReport(props,i){ //props have to be a Grade.
    return(<tr>
                <td>{i}</td>
                <td>{props.getCode()}</td>
                <td>{props.getName()}</td>
                <td>{props.getUnits()} واحد</td>
                <td><p class="pass">{props.getStatus()}</p></td>
                <td class="pass2">{props.getGrade()}</td>
           </tr>);
}

function termInReport(props,i){ //props have to be a Term.
    var i;
    var res=(<legend> {i} ترم </legend>
                <table>);
    for(i=0;i<props.getGrades().size();i++){
        res=res+courseInReport(props.getGrades()[i],i);
    }
    res=res+(<p class="avg">معدل: {props.getAvg()}</p>
          </table>);
    return res;
}

function report(props){ //props have to be a Student.
    var i;
    var res=(<legend> کارنامه </legend>);
    for(i=12;i>0 && props.getTerms()[i].getCourses().size()>0;i--){
        res=res+termInReport(props.getTerms()[i],i);
    }
    return res;
}